import email
from django.db import models
from django.conf import settings

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=100,blank=False)
    last_name = models.CharField(max_length=100,blank=False)
    email = models.EmailField(blank=False,max_length=50)
    password = models.CharField(max_length=50)
    username= models.CharField(max_length=30,unique=True, blank=True, null=True)
    birth_year = models.DateField(null=True)

    USERNAME_FIELD = 'email'



class Customer(models.Model):
    email = models.CharField(max_length=30,blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Content_owner(models.Model):
    email = models.CharField(max_length=30,blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Admin(models.Model):
    email = models.CharField(max_length=30,blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField()
    image = models.ImageField(upload_to='pics')

class Cart(models.Model):
    quantity = models.IntegerField()
    total_price = models.IntegerField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

class Orders(models.Model):
    order_date = models.DateField()
    order_time = models.TimeField()
    users = models.ManyToManyField(User)

class Payment(models.Model):
    mode_of_payment = models.CharField(max_length= 100)
    payment_date = models.DateField()
    payment_time = models.TimeField()
    details = models.ManyToManyField(User,related_name="payments", through='Payment_Details')

class Payment_Details(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    payment = models.ForeignKey(Payment, on_delete=models.CASCADE)
    telephone_number = models.IntegerField()
    card_number = models.CharField(blank= True,max_length= 30)

class Image(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='images')
    price = models.IntegerField()

    def __str__(self):
        return self.title










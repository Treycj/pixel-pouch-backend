import json
import site
import requests
from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import ImageForm
# from django.contrib.sites.models import Site
from django.http import HttpResponseRedirect
from django.shortcuts import render
from rest_framework.decorators import (api_view, permission_classes,renderer_classes)
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer

from rest_framework.views import APIView
from django.http.response import JsonResponse

from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm

from rest_framework import viewsets
from rest_framework.response import Response

from django.views import generic
from django.urls import reverse_lazy


from pixel.models import Admin, Cart, Content_owner, Customer, Image, Orders, Payment, Payment_Details, Product, User
from pixel.serializers import AdminSerializer, CartSerializer, Content_ownerSerializer, CustomerSerializer, OrdersSerializer, Payment_DetailsSerializer, PaymentSerializer, ProductSerializer, UserSerializer

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
   queryset = User.objects.all()
   serializer_class = UserSerializer

class CustomerViewSet(viewsets.ModelViewSet):
   queryset = Customer.objects.all()
   serializer_class = CustomerSerializer

class Content_ownerViewSet(viewsets.ModelViewSet):
   queryset = Content_owner.objects.all()
   serializer_class = Content_ownerSerializer

class AdminViewSet(viewsets.ModelViewSet):
   queryset = Admin.objects.all()
   serializer_class = AdminSerializer

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class CartViewSet(viewsets.ModelViewSet):
   queryset = Cart.objects.all()
   serializer_class = CartSerializer

class OrdersViewSet(viewsets.ModelViewSet):
   queryset = Orders.objects.all()
   serializer_class = OrdersSerializer

class PaymentViewSet(viewsets.ModelViewSet):
   queryset = Payment.objects.all()
   serializer_class = PaymentSerializer

class Payment_DetailsViewSet(viewsets.ModelViewSet):
   queryset = Payment_Details.objects.all()
   serializer_class = Payment_DetailsSerializer

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"


@api_view(('GET', 'POST'))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
@permission_classes([AllowAny])
def reset_user_password(request, **kwargs):
    # uses djoser to reset password
    if request.POST:
        current_site = site.objects.get_current()
        #names of the inputs in the password reset form
        password = request.POST.get('new_password')
        password_confirmation = request.POST.get('password_confirm')
        #data to accept. the uid and token is obtained as keyword arguments in the url
        payload = {
            'uid': kwargs.get('id'),
            'token': kwargs.get('Bearer'),
            'new_password': password,
            'password_confirm': password_confirmation
        }

        djoser_password_reset_url = 'auth/users/reset_password_confirm/'
        protocol = 'https'
        headers = {'content-Type': 'application/json'}
        if bool(request) and not request.is_secure():
            protocol = 'http'
        url = '{0}://{1}/{2}'.format(protocol, current_site,
                                     djoser_password_reset_url)
        response = requests.post(url,
                                 data=json.dumps(payload),
                                 headers=headers)

        if response.status_code == 204:
            # Give some feedback to the user.
            messages.success(request,
                             'Your password has been reset successfully!')
            return HttpResponseRedirect('/')
        else:
            response_object = response.json()
            response_object_keys = response_object.keys()
            #catch any errors
            for key in response_object_keys:
                decoded_string = response_object.get(key)[0].replace("'", "\'")
                messages.error(request, f'{decoded_string}')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
      # if the request method is a GET request, provide the template to show. in most cases, the password reset form.
    else:
        return render(request, 'templates/registration/reset_password.html')


def image_upload_view(request):
    """Process images uploaded by users"""
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # Get the current instance object to display in the template
            img_obj = form.instance
            return render(request, 'upload.html', {'form': form, 'img_obj': img_obj})
    else:
        form = ImageForm()
    return render(request, 'upload.html', {'form': form})




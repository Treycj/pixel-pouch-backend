"""pouch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static 

from django.contrib import admin
from django.urls import include,path


from rest_framework import routers
from rest_framework.authtoken import views

from pixel.views import AdminViewSet, CartViewSet, Content_ownerViewSet, CustomerViewSet, OrdersViewSet, Payment_DetailsViewSet, PaymentViewSet, ProductViewSet, SignUpView, UserViewSet, image_upload_view, reset_user_password


router = routers.DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'customer', CustomerViewSet)
router.register(r'content_owner', Content_ownerViewSet)
router.register(r'admin', AdminViewSet)
router.register(r'product', ProductViewSet)
router.register(r'cart', CartViewSet)
router.register(r'orders', OrdersViewSet)
router.register(r'payment', PaymentViewSet)
router.register(r'payment_details', Payment_DetailsViewSet)


urlpatterns = [
    path('adminS/', admin.site.urls),
    path('upload/',image_upload_view,name="image_upload"),
    path('', include(router.urls)),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', views.obtain_auth_token, name='api-token-auth'),
    # path('password/reset/<str:uid>/<str:token>/',reset_user_password,name='reset_user_password'),
    # path("pixel/", include("django.contrib.auth.urls")),
    path("signup/", SignUpView.as_view(), name="signup"),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
